module.exports = (sequelize, DataTypes) => {
    const Vehicles = sequelize.define("vehicles", {
        carId: {
            type: DataTypes.INTEGER,
        },
        carCode: {
            type: DataTypes.STRING,
        },
        carNumber: {
            type: DataTypes.STRING,
        },
        v_licence: {
            type: DataTypes.STRING
        },
        driver_id: {
            type: DataTypes.INTEGER
        },
        driver_fname: {
            type: DataTypes.STRING
        },
        driver_lname: {
            type: DataTypes.STRING
        },
        driver_mobile1: {
            type: DataTypes.STRING
        },
        v_size: {
            type: DataTypes.INTEGER
        },
        v_seats: {
            type: DataTypes.INTEGER
        },
        v_latitude: {
            type: DataTypes.STRING(15)
        },
        v_longitude: {
            type: DataTypes.STRING(15)
        },
        is_active: {
            type: DataTypes.INTEGER
        }
    });
    return Vehicles;
};