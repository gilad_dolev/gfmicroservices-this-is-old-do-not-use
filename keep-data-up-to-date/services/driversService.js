module.exports = (function (db) {
    return {
        addBulkDrivers
    };

    function addBulkDrivers(data) {
        return db.drivers.bulkCreate(data)
    }

})
