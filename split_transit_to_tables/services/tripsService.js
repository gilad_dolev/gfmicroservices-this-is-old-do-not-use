const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addTrip,
    };

    function addTrip(data) {
        return db.trips.create(data)
    }
})
