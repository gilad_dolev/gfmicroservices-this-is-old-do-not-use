module.exports = (sequelize, DataTypes) => {
    const Trips = sequelize.define("trips", {
        company_Id: {
            type: DataTypes.INTEGER
        },
        client_code: {
            type: DataTypes.INTEGER
        },
        start_time: {
            type: DataTypes.TIME
        },
        end_time: {
            type: DataTypes.TIME
        },
        line_code: {
            type: DataTypes.INTEGER
        },
        course_code: {
            type: DataTypes.INTEGER
        },
        line_description: {
            type: DataTypes.STRING
        },
        stations: {
            type: DataTypes.TEXT
        },
    });

    // Trips.associate = function (models) {

    //     Trips.belongsToMany(models.resources, {
    //         through: models.trips_to_resource,
    //         constraints: false
    //     });
    //     Trips.hasMany(models.trips_to_resource, { constraints: false })
    // }

    return Trips;
};